---
title: "Projet middleware - description"
author: Sébastien Bleichner, Louis-Baptiste Sobolewski & Julien Tavernier
geometry: margin=2cm
date: Lundi 28 mars 2022
fontsize: 12pt
mainfont: 'EB Garamond'
monofont: 'Input'
---
[//]: # (while inotifywait -e close_write rendu-description.md; do pandoc --pdf-engine=xelatex -V lang=fr ./rendu-description.md -o ./rendu-description.pdf; done&)

# Java-IRC

## Objectifs principaux et fonctionnalités

JIRC est un programme de chat inspiré du protocole IRC.

Les utilisateurs se connectent sans moyen d'authentification dans des salles afin d'échanger sur un sujet, à partir de messages en texte simple.

### Minimum viable

#### Client

 - un utilisateur peut se connecter à un serveur
 - il peut lister les utilisateurs connectés et les salles actives (>1 utilisateur dans la salle)
 - il peut rejoindre UNE salle (pas plus), en recevoir les messages et y en envoyer (mais pas en voir l'historique)

#### Serveur

 - le serveur affiche les événements mais pas les messages échangés
 - il peut lister les utilisateurs connectés et les salles actives
 - il peut voir les utilisateurs d'une salle, et la salle d'un utilisateur

### Fonctionnalités additionnelles

 - les clients peuvent se connecter à plusieurs salles en simultané
 - un historique des messages est conservé et chargé à l'entrée dans une salle
 - les utilisateurs peuvent échanger des messages privés entre eux
 - le serveur peut envoyer un message à tous les utilisateurs
 - les historiques sont enregistrés par le serveur sur le disque dur

\pagebreak

## Architecture

Le serveur est en deux parties : un serveur Glassfish qui sert d'implémentation JMS, et une application RMI qui distribue notamment le tuple space.

### JMS et salles de chat

JMS aura peut-être un topic permanent selon l'avancement du projet pour que le serveur puisse envoyer un message à tous les clients par exemple ; mais le plus commun sera des topics temporaires, créés dynamiquement par le serveur afin de maintenir la connexion, qui représenteront les salles de chat.

Chaque client se connectant à une salle s'abonnera au topic temporaire portant le nom de cette salle. Le topic transportera 3 types de messages : les messages échangés par les utilisateurs dans la salle, les entrées et sorties d'utilisateurs dans la salle.

Si la fonctionnalité d'historique est implémentée, le serveur écoutera tous les topics et ce sera son rôle de stocker les messages échangés dans le tuple space.

### RMI et tuple space

Dans la version minimale de l'application, le registre de tuples spaces contient deux espaces :

 - `users`, avec des tuples de la forme `(nickname)` pour savoir qui est connecté au serveur ;
 - `rooms`, avec des tuples de la forme `(room, nickname)` pour savoir qui est entré dans quelle salle (ainsi que les salles actives).

Le RMI sert à rendre ces tuples spaces accessibles à tous les clients, puisqu'il s'agit d'une mémoire partagée.
