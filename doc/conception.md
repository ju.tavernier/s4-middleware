# Projet middleware S4

Logiciel de chat type IRC

Nom du `RemoteServer` dans le registre RMI : `server`

Tuple spaces (toutes les valeurs sont des String) :

 - `users` : (nickname)
 - `rooms` : (room name, nickname)

## Client

```terminal
bibi: hi folks





-------------------------------------------------------------
You are in room #room
You haven't issued any command yet
> 
```

### Commandes

 - `/help` : liste les commandes possibles
 - `/connect nickname address port` : sélectionner un pseudo et rejoindre un serveur
 - `/join #room` : rejoindre une room
 - `/leave #room` : quitter une room
 - `/list rooms` : lister les rooms actives (au moins un utilisateur)
 - `/list users` : lister les utilisateurs connectés (pas forcément dans une room)
 - `/disconnect` : se déconnecter du serveur
 - `/exit` : quitter

## Serveur

```terminal
[info] login -> bibi
[info] join -> bibi #room
[warn] login -> bibi already in use


-------------------------------------------------------------
1 connected user, 1 active room, 1 warning issued, 0h1m uptime
You haven't issued any command yet
> 
```

### Commandes

 - `which user` : dans quelle room est l'user
 - `who #room` : quels users sont dans cette room
 - `rooms` : lister les rooms actives
 - `users` : lister les utilisateurs connectés

## Middlewares

### JMS

Événements :

 - utilisateur rejoint la salle
 - utilisateur quitte la salle
 - utilisateur envoie un message dans la salle

### Tuple space

(String username, String room)

### RMI

Pour distribuer le tuple space et le JMS à tout le monde

## Features supplémentaires

 - connexion à plusieurs rooms
 - persistence des messages dans les rooms à travers les sessions
 - le serveur ping les clients pour vérifier s'ils ne sont pas partis
 - le serveur broadcast un message à tous les utilisateurs
 - PMs
 - persistence des PMs à travers les sessions
 - persistence des trucs persistents à travers les redémarrages serveur
