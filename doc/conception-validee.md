# Composants

Toute opération où le token est invalide retourne null

Le token contient :

 - le nom d'utilisateur

## RMI

RemoteServer `server` :

 - authentification (mots de passe en sha512)
 - opérations demandant écriture en tuple space

TupleSpaceRegistry `db` :

 - accès en lecture uniquement aux TupleSpace
 - historique des salles

## TS(R)

 - lecture seule pour les clients
 - historique des messages : `room-<nom salle>`
   - tuples `(<timestamp unix>, <nom utilisateur>, <message>)`
 - utilisateurs connectés : `users`
   - tuples `(<nom d'utilisateur>)`
 - utilisateurs connectés dans les rooms : `rooms`
   - tuples `(<nom salle>, <nom utilisateur>)`

## JMS

Un seul topic sur lequel toutes les salles passent

Messages format `<nom salle>|<type événement>|<sujet événement>[|<message>]`

Trois messages possibles : rejoindre, quitter, message

 - `<nom salle>|<nom utilisateur>|join`
 - `<nom salle>|<nom utilisateur>|leave`
 - `<nom salle>|<nom utilisateur>|message|<message>`

# Client

 - `/help` : liste les commandes possibles
 - `/connect address port` : rejoindre un serveur
 - `/register username password` : s'enregistrer auprès du serveur
 - `/auth username password` : s'authentifier auprès du serveur
 - `/join room` : rejoindre une room
 - `/leave room` : quitter une room
 - `/list rooms` : lister les rooms actives (au moins un utilisateur)
 - `/list users` : lister les utilisateurs connectés (pas forcément dans une room)
 - `/disconnect` : se déconnecter du serveur
 - `/exit` : quitter

# Serveur

 - `which user` : dans quelle room est l'user
 - `who #room` : quels users sont dans cette room
 - `rooms` : lister les rooms actives
 - `users` : lister les utilisateurs connectés

# Déroulé d'une session

## Authentification

Demande un token auprès du serveur

> RMI.auth(username, password) retourne Token

## Connexion

Notifier le serveur

> RMI.connect(token)

## Entrée dans une salle

Demander l'entrée en salle au serveur

> RMI.join(token, room) retourne bool

## Post d'un message

Envoyer un message dans la salle

> RMI.sendMessage(token, room, message) retourne bool

## Sortie de la salle

Notifier le serveur

> RMI.leave(token, room) retourne bool

## Déconnexion

Notifier le serveur

> RMI.disconnect(token)
