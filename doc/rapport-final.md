---
title: "Compte rendu - Intergiciels"
author: Sébastien Bleichner, Louis-Baptiste Sobolewski, Julien Tavernier
geometry: margin=2cm
date: Lundi 25 avril 2022
fontsize: 12pt
mainfont: 'EB Garamond'
monofont: 'Input'
---
[//]: # (while inotifywait -e close_write rapport-final.md; do pandoc --pdf-engine=xelatex -V lang=fr ./rapport-final.md -o ./rapport-final.pdf; done&)

# 1. Java-IRC

JIRC est un programme de chat inspiré du protocole IRC. Les utilisateurs se connectent dans des salles afin d’échanger sur un sujet à partir de messages en texte simple.

Les IRC originaux ne demandaient pas d'authentification afin de participer aux discussions ; cependant, pour respecter la contrainte d'usage de 3 intergiciels différents, nous avons choisi d'instaurer l'authentification.

L'idée est que l'utilisateur se connecte à un serveur, y crée un compte puis s'authentifie. Le client reçoit alors un jeton, qu'il émettra au serveur à chaque communication afin que son identité soit garantie ; l'usurpation n'est pas possible comme ça le serait sur un IRC classique.

Même si cela n'est pas implémenté, le client pourrait tout à faire lire l'historique des salles de chat sans être authentifié (sans aucune modification du serveur) ; la lecture est publique, l'écriture demande de prouver son identité.

**Compilation**

Les instructions de compilation sont partie 3. Au cas où, des binaires sont fournis dans le rendu.

**Source**

[https://forgens.univ-ubs.fr/gitlab/e2004252/s4-middleware](https://forgens.univ-ubs.fr/gitlab/e2004252/s4-middleware)

**Démo**

[https://volumen.univ-ubs.fr/thg3td7i](https://volumen.univ-ubs.fr/thg3td7i)

\pagebreak

# 2. Utilisation des middlewares

Nous avons choisi de faire usage des middlewares suivants pour notre projet :

 - le tuple space sert de base de données avec un paradigme intéressant et original ;
 - le JMS semble intuitif et logique pour une application de messagerie instantanée ;
 - les deux middlewares précédents étant plutôt libre d'accès par les clients, le RMI sert de glue et gère les accès.

Nous allons détailler l'usage fait de ces middlewares.

## Tuple space

Le tuple space sert de base de données à l'application. Son accès sera détaillé dans la partie RMI ; nous allons pour l'instant détailler les différents tuple spaces utilisés.

### Utilisateurs connectés

Un espace nommé `users` sert à stocker les utilisateurs qui sont connectés (et authentifiés) au serveur.

> Les tuples de cet espace sont de la forme `(<nom utilisateur>)`.
    
### Utilisateurs dans les salles

Un espace nommé `rooms` sert à savoir quelles salles sont actuellement peuplées d'utilisateurs et peuvent être considérées actives.

> Les tuples de cet espace sont de la forme `(<nom salle>, <nom utilisateur>)`.

Ce format permet d'obtenir très facilement diverses informations avec des requêtes simples (le caractère `?` est un joker) :

 - `(<nom salle>, ?)` permet de savoir si une salle est active et d'obtenir la liste de ses utilisateurs ;
 - `(?, <nom utilisateur>)` permet de savoir dans quelle salle se trouve un utilisateur (pour retrouver un ami dans une salle par exemple).

### Historique des messages

Un nombre indéterminé d'espaces nommés `room-<nom salle>` permettent de stocker l'historique des messages de chaque salle.

> Les tuples de ces espaces sont de la forme `(<timestamp unix>, <nom utilisateur>, <contenu du message>)`.

La présence d'un timestamp permet aux clients de reconstruire et d'afficher l'historique des messages dans le bon ordre lorsqu'un utilisateur rejoint une salle.

## JMS

Le JMS sert à avertir les utilisateurs de l'activité des salles en temps réel. Le modèle utilisé est donc celui de topic : les messages envoyés doivent arriver à plusieurs consommateurs.

L'idée de départ était la suivante : lorsqu'une salle devient active, un topic temporaire est créé et une connexion à celui-ci est distribuée à tous les clients rejoignant la salle. Cependant, cette idée s'est révélée compliquée (voire impossible ?) à mettre en place.

Nous avons donc opté pour un topic "fixe" créé directement sur Glassfish où tous les messages de toutes les salles passent. Cela poursuit la philosophie du public en lecture. Même si elle est plutôt simple à mettre en place, cette approche a deux inconvénients :

 - tout le monde peut produire des messages dans le topic. Ceci est d'une moindre importance, car même si des messages "faux" sont envoyés par le biais de JMS, ils ne seront pas stockés dans le tuple space ; l'historique restera sain.
  - si un serveur présente une activité importante, des problèmes de performances (client et serveur, car le client doit parser tous les messages) et d'utilisation du réseau pourraient se poser.

À la réception d'un message, le principe est que le client vérifie si le message concerne la salle dans laquelle se trouve l'utilisateur actuellement ; si tel est le cas, le message est traité et affiché, sinon ignoré.

3 types de messages (transportant du texte) circulent ainsi par JMS, et sont émis par le serveur uniquement :

### Entrée ou sortie d'une salle

Un message est distribué chaque fois qu'un utilisateur entre dans une salle ou la quitte :

> `<nom salle>|<nom utilisateur>|join`

> `<nom salle>|<nom utilisateur>|leave`

### Message envoyé dans une salle

Un message est distribué chaque fois qu'un utilisateur écrit un message dans une salle :

> `<nom salle>|<nom utilisateur>|message|<message>`

## RMI

Le tuple space en tant que base de données et le JMS en tant que transmetteur de messages en temps réel peuvent être vus comme des fonctions support ; le RMI est en quelques sortes la pièce centrale qui unit toutes les autres.

Le serveur exporte un seul objet sur le registre RMI sous le nom de `server`. C'est par ce biais que les clients effectuent des actions sur le serveur.

### Services publics

Ces services sont accessibles à n'importe quel client connecté au serveur, sans que l'utilisateur n'ait précisé ses identifiants.

 - **Informations sur le serveur**
 
   Les méthodes `getDisplayName` et `getMotD` permettent respectivement d'obtenir le nom du serveur ainsi que le "Message of the Day" qui s'affiche à la connexion.

   Il ne s'agit pas là de méthodes d'une grande utilité, mais permettent de retrouver l'ambiance d'un serveur IRC.

 - **Obtention d'un tuple space**

   La méthode `getDB` permet d'obtenir un tuple space par son nom. En effet, pour des raisons détaillées dans la partie 4, le serveur distribue les tuple spaces directement par le RMI au lieu de distribuer le registre de tuple spaces.

   Par ce biais, tout utilisateur non authentifié peut obtenir les tuples `users`, `rooms` et `room-<nom salle>` et toutes les informations qui en découlent, détaillées dans la partie précédente.

 - **Création d'un compte**

   La méthode `register` permet aux clients connectés de créer un compte sur le serveur. Les comptes sont utiles pour réserver un pseudonyme et s'assurer de ne pas être usurpé.
 
   Les mots de passe saisis en clair par l'utilisateur ne vont jamais plus loin que la partie client ; le mot de passe transitant par le réseau et sauvegardé par le serveur est en réalité un hash SHA-512.

 - **Authentification**

   La méthode `auth` permet aux client connectés d'obtenir un jeton d'authentification représentant un compte utilisateur, ce qui donne accès aux services détaillés ci-après.

### Services sur authentification

Ces services sont disponibles aux utilisateurs qui se sont authentifiés et disposent d'un token valide. Les tokens sont expliqués dans la partie 4.

 - **Rejoindre ou quitter une salle**

   Avant de pouvoir publier un message dans une salle, un utilisateur doit annoncer qu'il rejoint cette salle par la fonction `join`. Le serveur l'inscrit alors dans le tuple space `rooms` en tant qu'utilisateur actif de cette salle.

   L'utilisateur peut également annoncer qu'il quitte la salle par la fonction `leave`, le tuple correspondant dans `rooms` sera alors supprimé.

 - **Envoyer un message dans une salle**

   La fonction `sendMsg` permet à un utilisateur d'envoyer un message dans une salle. Il doit avoir rejoint la salle.

 - **Se déconnecter**

   La fonction `disconnect` permet à un utilisateur d'annoncer qu'il quitte le serveur, il est donc désinscrit du tuple space `users`.

\pagebreak

# 3. Système de compilation et librairies

Nous aurions voulu configurer un projet Maven de la façon suivante :

 - un projet parent avec trois sous projets pour le client, le serveur et le code commun aux deux
 - la compilation résulte en deux fichiers JAR exécutables et indépendants, c'est à dire contenant les librairies utilisées (librairies externes et sous projet contenant le code commun)

Nous n'y sommes cependant pas parvenus. À la place, nous avons utilisé lors du développement une solution hybride :

 - nous utilisions VS Code comme IDE. Une de ses extensions pour le Java permet de compiler automatiquement tout fichier .java édité (en plus de proposer l'autocomplétion sur les librairies `.jar` fournies). Le projet était donc constamment compilé et à jour dans un dossier `bin` contenant les fichiers `.class` et respectant la hiérarchie des packages.
 - VS Code dispose d'un système de tâches ; nous avons créé une tâche qui appelle le script `build.sh` afin de créer deux fichiers JAR interactifs. Cela passe par le dézippage des librairies JAR, la copie de tous les fichiers `.class` dans un dossier temporaire et la création des deux JAR en précisant la classe d'entrée.

Afin de ne pas reposer sur l'IDE pour le rendu final, le script `build.sh` a été complété et gère également la compilation du code du projet. Ainsi, pour compiler le projet et obtenir les deux JAR exécutables (client et serveur), il suffit de lancer ce script en se plaçant dans le dossier racine du projet.

Il est important d'utiliser Java 8, car au-dessus `javaee-api` ne fonctionne plus, et en-deça les streams n'existent pas encore. L'implémentation du JMS utilisée est Glassfish 5 ; nous faisons usage de la "Connection Factory" par défaut, et il faut créer une ressource de destination dont le nom JNDI est `jms/jirc` et le type `javax.jms.Topic`.

Un mot sur les librairies utilisées :

 - `javaee-api` permet d'utiliser l'implémentation du JMS proposée par Glassfish 5 ;
 - `lanterna` permet de construire des Terminal User Interface (hybride entre une application en terminal et une application graphique) ;
 - `java-jwt` permet de créer et valider des JSON Web Tokens (détaillés plus bas) ;
 - `jackson-core`, `jackson-annotations` et `jackson-databind` sont des dépendances de `java-jwt`.

\pagebreak

# 4. Détail de l'implémentation

## Interface utilisateur

L'interface en terminal est faite à l'aide de la librairie Lanterna. Cette librairie propose plusieurs niveaux d'abstraction du terminal :

 - déplacement du curseur à n'importe quelles coordonnées (ligne/colonne) du terminal, écriture caractère par caractère ;
 - écriture de chaînes entières à n'importe quelles coordonnées, gestion des couleurs de fond et de texte, lecture du clavier, événement au redimensionnement de la fenêtre...
 - système d'interface graphique par widgets

Nous avons choisi l'interface graphique par widgets, qui était la plus adaptée à nos besoins (même si nous aurions préféré des couleurs... moins clinquantes).

L'implémentation de l'interface graphique est la classe `fr.ensibs.jirc.commons.view.View` ; en effet, les interfaces du client et du serveur étant identiques, il faisait sens de placer la classe dans `commons`.

L'interface `ViewInputListener` est destinée à être implémentée par les classes principales des applications afin que la partie graphique puisse envoyer la saisie de l'utilisateur à la partie logique.

### Décomposition de l'interface

L'interface est composée de haut en bas :

 - d'une liste pour afficher des événements ou des messages de façon chronologique `ActionListBox`. C'est le widget préfait le plus proche de ce dont on a besoin ; chaque élément doit avoir un `Runnable` qui est exécuté en guide d'action, nous passons simplement un Runnable vide. Nous avons également fait en sorte que lorsque les éléments dépassent la capacité de l'écran, la liste montre les éléments les plus en bas/les plus récents.

 - d'un `Label` servant à indiquer le statut de l'application (serveur : nombre de personnes connectées... client : connecté, authentifié, dans une salle...)

 - d'un `Label` servant à indiquer le résultat d'une commande de l'utilisateur

 - d'une `EnterableTextBox` où l'utilisateur écrit des messages ou des commandes et valide sa saisie par un appui sur la touche Entrée.

### EnterableTextBox

L'`EnterableTextBox` n'est pas un widget par défaut de Lanterna, mais un que nous avons créé. Il hérite de la `TextBox` de Lanterna, et permet de définir un `Runnable` à exécuter lorsqu'un appui sur la touche Entrée est fait alors que le focus est sur ce widget.

## Tuple space

Les parties communes du tuple space se trouvent dans `fr.ensibs.jirc.commons.tuplespace`. On trouve :

 - l'implémentation du `Tuple` et du `Template` ;
 - l'interface du `TupleSpace`.

L'interface du `TupleSpace` a la particularité de ne proposer que la méthode `read` ; en effet, les tuple spaces sont en lecture seule pour les clients. C'est donc dans l'implémentation du tuple space qu'on trouvera les méthodes d'écriture.

### Implémentation

L'implémentation du tuple space se trouve dans `fr.ensibs.jirc.server.tuplespace`. On trouve :

 - `TupleSpaceImpl` qui, en plus d'implémenter le `read`, ajoute le `take` pour pouvoir enlever des tuples du tuple space ; le `add` pour ajouter des tuples dans le tuple space.
 - `TupleSpaceRegistry` qui dispose d'une `HashMap<String, TupleSpace>` afin de stocker les tuple spaces déjà créés. En effet, on a ici un semblant de pattern singleton : si un TupleSpace demandé n'existe pas, il est créé à la volée ; s'il existe déjà, il est retourné.

### `TupleSpaceRegistry`

Le registre de tuple spaces n'est pas mis à disposition des clients. En effet, le serveur doit pouvoir utiliser les `TupleSpace` directement, tandis que les clients se voient transmettre un proxy RMI vers les `TupleSpace` ; ainsi le `TupleSpaceRegistry` distribue des `TupleSpace` directs, et la méthode `getDB` du serveur en fait des proxy à la demande des clients.

## Authentification

Lorsqu'un client appelle la méthode `auth` du serveur en spécifiant des identifiants valides, le serveur retourne une chaîne de caractères représentant un JSON Web Token.

Un JWT est un token signé par le serveur, que le client va utiliser pour s'identifier à chaque appel de l'API. Puisqu'il est signé par le serveur, le client ne peut pas le modifier et le serveur peut vérifier son authenticité.

Il peut contenir des données JSON : dans notre projet, nous utilisons le champ `sub` pour "subject", dans lequel nous stockons le nom de l'utilisateur à qui correspond le token.

Plus de détails sur les JWT ici : _[https://jwt.io/](https://jwt.io/)_

## Points d'entrée

Les points d'entrée des applications client et serveur sont `fr.ensibs.jirc.client.ClientApp` et `fr.ensibs.jirc.server.ServerApp`.

Dans les packages correspondant, on trouve une redéfinition de `fr.ensibs.jirc.commons.view.View` afin de l'adapter aux besoins de chaque partie ; la gestion des commandes se fait dans la partie `*App`.

La partie client contient une énumération `State` : elle permet de définir un semblant d'automate fini à 4 états, à savoir déconnecté, connecté, authentifié et dans une salle. La `ClientApp` s'articule donc autour de ces 4 états afin de savoir ce qu'il faut modifier pour changer d'état (par exemple passer de "dans une salle" à "déconnecté" directement) ou les variables qui sont définies ou nulles à un instant donné.

La partie serveur contient deux particularités :

 - `RemoteServerImpl` implémente l'interface `RemoteServer` de `commons`, c'est là que se fait l'interaction client-serveur ;
 - `AccountManager` est là pour stocker les couples nom d'utilisateur/hash de mot de passe et alléger la classe `ServerApp`. Séparer cette partie permet aussi de préparer le terrain pour pouvoir sérialiser et stocker l'objet sur le disque dur, afin d'assurer la persistance des comptes.

\pagebreak

# 5. Améliorations possibles

## Vérification des noms d'utilisateurs et de salles

Nous avons vu que les messages JMS sont des chaînes de caractères présentant plusieurs champs séparés par des pipes (`|`). Cependant, aucune vérification de saisie n'est faite sur les noms d'utilisateurs et les noms de salles ; mettre un pipe dans un nom est une façon très simple de casser le bon déroulement de l'application.

Il faudrait n'autoriser dans les noms que les lettres majuscules et minuscules et les chiffres, par exemple (en tout cas un jeu de caractères réduit). La vérification pourrait alors se faire par une expression régulière.

## Parcours d'un serveur sans authentification

Nous avons précisé plus haut qu'il est techniquement possible sans modifier le serveur d'accéder en lecture à la totalité de l'application. Cette fonctionnalité n'est pas implémentée dans le client actuellement.

## Architecture du code

Quelques points concernant l'architecture du code pourraient être améliorés :

 - La manière dont les commandes sont gérées actuellement fonctionne, mais n'est pas particulièrement agréable à maintenir. On pourrait imaginer un système ou une commande est un objet avec :

   - un champ pour le nom de la commande ("join", "connect"...)
   - une fonction qui correspond à l'exécution de la commande...
 
   Les commandes s'enregistreraient dans une Map au démarrage de l'application.

 - Nous avons essayé au mieux de séparé le thread principal du thread de l'interface graphique ; cet essai quelque peu infructueux se traduit par la présence de Runnable un peu partout dans le code. Un nettoyage est à faire ; éventuellement une file d'événements devrait être implémentée dans le thread principal.

## Mots de passe

Les mots de passe sont actuellement hachés en SHA-512.

L'idéal serait d'ajouter un sel avant le hachage, et d'utiliser un algorithme de hachage lent conçu spécialement pour les mots de passe (`bcrypt` est un des plus connus).

## Ping régulier des clients

Actuellement, si un client plante, le serveur continue de fonctionner comme si l'utilisateur derrière ce client était inactif. Même si l'utilisateur se reconnecte, il ne peut plus rejoindre la salle dans laquelle il était lors du plantage par exemple.

Il faudrait que le serveur envoie une requête à tous les clients à intervalles réguliers afin de savoir s'ils sont toujours actifs ; si tel n'est pas le cas, l'utilisateur serait considéré déconnecté.

Cela passerait certainement par un objet exposé par chaque client par le biais du RMI qui disposerait d'une fonction `ping` de type `void`, simplement pour vérifier que le serveur est capable de l'appeler.

## Persistance des données

Lorsque le serveur ferme, tout est oublié : comptes créés, historique des messages... Il serait intéressant que les tuple spaces `room-<nom salle>` et la liste des comptes utilisateurs soient sauvegardés sur le disque dur afin d'être rechargés au démarrage suivant.

## Historique des messages potentiellement infini

L'historique des messages n'a actuellement pas de limite. Cela posera un problème de mémoire vive côté serveur, et de lenteur lors du chargement de l'historique côté client.

Dans l'idéal, la fonctionnalité d'historique ne conserverait que les `n` derniers messages de chaque salle (et éventuellement, au delà d'un certain nombre de salles, les historiques des salles les moins actives sont oubliés).

## Messages privés

La fonctionnalité de messages privés était prévue mais nous n'avons malheureusement pas eu le temps de l'implémenter. Il est probable qu'une partie de l'application actuelle devrait être re-conçue afin d'intégrer les messages privés au mieux.

## Fonctions de modération

Les exemples d'abus sur internet dès lors que les personnes se pensent anonymes de manquent pas. Le protocole IRC définit des permissions et rôles de modération ; un modérateur peut alors bannir temporairement un utilisateur ou une adresse IP par exemple.
