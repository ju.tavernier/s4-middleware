package fr.ensibs.jirc.commons.tuplespace;

import java.io.Serializable;

/**
 * An immutable finite ordered sequence of values.
 * 
 * This class represents a passive tuple, which {@link #exec()} method doesn't
 * do anything.
 */
public class Tuple implements Serializable
{
    /**
     * Tuple's values
     */
    private final Serializable[] values;

    /**
     * Constructor
     * 
     * @param values tuple's values
     */
    public Tuple(Serializable... values)
    {
        this.values = values;
    }

    /**
     * Number of values in the tuple
     *
     * @return number of values
     */
    public int size()
    {
        return values.length;
    }

    /**
     * Tuple's n_th value
     *
     * @param index index of the requested value
     * @return value at the given index
     */
    public Serializable get(int index)
    {
        return values[index];
    }
    
    /**
     * Execute this tuple in a tuple space.
     * Since this is a passive implementation, exec() does nothing but in an
     * overriden version, one could for instance increment a value.
     *
     * @return new tuple produced by this execution
     */
    public Tuple exec()
    {
        return this;
    }

    /**
     * Creates a string representation of the object.
     * Formatted as "[value_1, ..., value_n]".
     * 
     * @return string representation of the tuple
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < size(); i++) {
            sb.append(get(i));
            if (i < size() - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
