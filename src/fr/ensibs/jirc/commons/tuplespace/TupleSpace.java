package fr.ensibs.jirc.commons.tuplespace;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * A shared associative memory, or a collection of tuples
 * Not true to Linda's tuple space but tailored to JIRC needs
 */
public interface TupleSpace extends Remote
{
    /**
     * Non-destructively reads a tuples in space.
     *
     * @param template a template that the returned tuples should match
     * @return list of tuples that match the given template
     */
    List<Tuple> read(Template template) throws RemoteException;
}
