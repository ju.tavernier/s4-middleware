package fr.ensibs.jirc.commons.tuplespace;

import java.io.Serializable;
import java.util.Objects;

/**
 * A tuple that contains wildcard values
 * A template is used in read operations to a tuple space
 */
public class Template extends Tuple
{
    /**
     * Wildcard token
     */
    public static final String WILDCARD = "?";

    /**
     * Constructor, just calls {@link Tuple#Tuple(Serializable...)}
     *
     * @param elements the template elements
     */
    public Template(Serializable... elements)
    {
        super(elements);
    }

    /**
     * Check whether the given tuple matches the template, in other words the
     * template and the tuple have the same number of elements and the non
     * wildcard values of the template are equal to the values in the tuple at
     * the same index
     *
     * @param tuple tuple to be compared to the current template
     * @return true if the given tuple matches the current template
     */
    public boolean matches(Tuple tuple)
    {
        // first condition: size matches
        if (tuple.size() == this.size()) {
            // second condition: values match
            for (int i = 0; i < this.size(); i++) {
                Serializable value = this.get(i);
                if (!WILDCARD.equals(value) && !Objects.equals(value, tuple.get(i))) {
                    // return false if a non-match has been found
                    return false;
                }
            }
            // return true when all values have been tested
            return true;
        }
        return false;
    }
}
