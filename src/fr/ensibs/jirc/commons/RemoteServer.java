package fr.ensibs.jirc.commons;

import java.rmi.Remote;
import java.rmi.RemoteException;

import fr.ensibs.jirc.commons.tuplespace.TupleSpace;

public interface RemoteServer extends Remote {

    /**
     * Server's display name
     * @return server's auto-declared name
     * @throws RemoteException
     */
    String getDisplayName() throws RemoteException;

    /**
     * Server's message of the day
     * @return
     * @throws RemoteException
     */
    String getMotD() throws RemoteException;

    /**
     * Get tuple space from server
     * @return
     * @throws RemoteException
     */
    TupleSpace getDB(String name) throws RemoteException;

    /**
     * Creates an account on the server
     * @param username
     * @param password
     * @return false if account couldn't be created
     */
    boolean register(String username, byte[] password) throws RemoteException;

    /**
     * Generate a token to use API functions and register as connected user
     * @param username
     * @param password
     * @return valid Token associated to username, null if invalid credentials
     */
    String auth(String username, byte[] password) throws RemoteException;

    /**
     * Register user in the room and subscribe to events
     * @param token
     * @param room
     * @return JMS connection, null if invalid token/user already in room
     */
    boolean join(String token, String room) throws RemoteException;

    /**
     * Send a message to a room
     * @param token
     * @param room
     * @param msg
     * @return false if invalid token/user not in room
     */
    boolean sendMsg(String token, String room, String msg) throws RemoteException;

    /**
     * Unregister from room
     * @param token
     * @param room
     * @return false if invalid token/user not in room
     */
    boolean leave(String token, String room) throws RemoteException;

    /**
     * Unregister as connected user
     * @param token
     * @return false if invalid token/user not connected
     */
    boolean disconnect(String token) throws RemoteException;

}
