package fr.ensibs.jirc.commons;

public class ExceptionHandler {
    
    public static void exit(Exception e, String msg) {
        System.err.println(msg);
        e.printStackTrace();
        System.exit(1);
    }

    public static void connectionFailed(Exception e) {
        exit(e, "Connection failed");
    }

}
