package fr.ensibs.jirc.commons.view;

import java.io.IOException;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.AbsoluteLayout;
import com.googlecode.lanterna.gui2.ActionListBox;
import com.googlecode.lanterna.gui2.AsynchronousTextGUIThread;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.MultiWindowTextGUI;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.SeparateTextGUIThread;
import com.googlecode.lanterna.gui2.Window;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;

import fr.ensibs.jirc.commons.ExceptionHandler;

/**
 * Wrapper for Lanterna API
 */
public abstract class View {

    // lanterna api objects
    private Screen screen;
    private WindowBasedTextGUI gui;
    private Window window;
    private Panel panel;

    // ui objects that will be used at runtime
    private Label cmdResult, status;
    private EnterableTextBox textbox;
    private ActionListBox logs;

    // communication with business object
    private ViewInputListener business;
    public final AsynchronousTextGUIThread thread;

    /**
     * ActionListBox items require an action, which we don't need so here is an
     * action that doesn't do anything
     */
    private static final Runnable RUNNOTHING =
        new Runnable() { @Override public void run() { } };

    /**
     * This class isn't intended to be used without being overriden at some
     * places, here is the message which will be displayed if not overriden
     */
    private static final String DONTUSETHISASIS = "Extend " + View.class.getName()
        + " for both Client and Server to adapt to each use case";



    /**
     * Gets Lanterna API objects, sets default UI text values and calls render()
     * @throws IOException
     */
    public View(ViewInputListener business) throws IOException {
        this.business = business;

        screen = (new DefaultTerminalFactory()).createScreen();
        gui = new MultiWindowTextGUI(new SeparateTextGUIThread.Factory(), screen);
        window = new BasicWindow();
        panel = new Panel(new AbsoluteLayout());
        window.setComponent(panel);
        gui.addWindow(window);

        buildUI();
        textbox.takeFocus();

        // start gui
        screen.startScreen();
        thread = (AsynchronousTextGUIThread) gui.getGUIThread();
        thread.start();
    }

    /**
     * Adds components to panel
     * @throws IOException
     */
    private void buildUI() throws IOException {
        TerminalSize size = screen.getTerminalSize();

        // commands prompt
        textbox = new EnterableTextBox(new Runnable() {
            @Override
            public void run() {
                // FIXME business' class code is running in GUI thread
                View.this.business.parseInput(textbox.getText());
                textbox.setText("");
            }
        });
        textbox.setSize(new TerminalSize(size.getColumns() - 6, 1));
        textbox.setPosition(new TerminalPosition(0, size.getRows() - 6));
        panel.addComponent(textbox);

        // commands result
        cmdResult = new Label("You haven't issued any command yet");
        cmdResult.setSize(new TerminalSize(size.getColumns() - 6, 1));
        cmdResult.setPosition(new TerminalPosition(0, size.getRows() - 7));
        panel.addComponent(cmdResult);

        // status message
        status = new Label(DONTUSETHISASIS);
        status.setSize(new TerminalSize(size.getColumns() - 6, 1));
        status.setPosition(new TerminalPosition(0, size.getRows() - 8));
        panel.addComponent(status);

        // messages list
        logs = new ActionListBox();
        logs.setSize(new TerminalSize(size.getColumns() - 6, size.getRows() - 9));
        logs.setPosition(new TerminalPosition(0, 0));
        panel.addComponent(logs);
    }

    /**
     * Call this before quitting application, otherwise terminal won't be
     * usable and GUI thread will persist
     */
    public void close() {
        try {
            screen.stopScreen();
        } catch (IOException e) {
            ExceptionHandler.exit(e, "Couldn't exit private mode");
        }

        ((AsynchronousTextGUIThread) gui.getGUIThread()).stop();
    }

    public void setStatus(String status) {
        this.status.setText(status);            
    }

    public void setCmdResult(String cmdResult) {
        this.cmdResult.setText(cmdResult);
    }

    public void addLog(String log) {
        this.logs.addItem(log, RUNNOTHING);
        // ActionListBox won't scroll itself, so set focus to the last item
        logs.setSelectedIndex(logs.getItemCount() - 1);
        // we don't wanna interrupt user's typing, so set back focus to textbox
        textbox.takeFocus();
    }

    public void clearLogs() {
        this.logs.clearItems();
    }
    
}
