package fr.ensibs.jirc.commons.view;

import com.googlecode.lanterna.gui2.TextBox;
import com.googlecode.lanterna.input.KeyStroke;

/**
 * Textbox which does something special upon "Enter" keystroke.
 * WARNING: doesn't implement all TextBox's constructors!
 */
public class EnterableTextBox extends TextBox {

    private Runnable enterAction;

    public EnterableTextBox(Runnable enterAction) {
        super();
        this.enterAction = enterAction;
    }
    
    @Override
    public synchronized Result handleKeyStroke(KeyStroke keyStroke) {
        switch(keyStroke.getKeyType()) {
            // special Enter handle
            case Enter:
                enterAction.run();
                return Result.HANDLED;
            // keep default handler for every other keystroke
            default:
                return super.handleKeyStroke(keyStroke);
        }
    }

}
