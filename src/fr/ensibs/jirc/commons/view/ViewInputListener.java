package fr.ensibs.jirc.commons.view;

/**
 * This interface is meant to be implemented by applications using View so they
 * can receive user inputs from the GUI thread
 */
public interface ViewInputListener {

    /**
     * View will call this function when user validates a command
     * @param input
     */
    void parseInput(String input);

}
