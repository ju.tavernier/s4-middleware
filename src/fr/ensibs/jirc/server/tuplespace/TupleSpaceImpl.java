package fr.ensibs.jirc.server.tuplespace;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import fr.ensibs.jirc.commons.tuplespace.Template;
import fr.ensibs.jirc.commons.tuplespace.Tuple;
import fr.ensibs.jirc.commons.tuplespace.TupleSpace;

public class TupleSpaceImpl implements TupleSpace {

    private List<Tuple> space;

    public TupleSpaceImpl() {
        space = new ArrayList<Tuple>();
    }

    public void add(Tuple tuple) throws RemoteException {
        space.add(tuple);
    }

    public List<Tuple> take(Template template) throws RemoteException {
        // get matching tuples
        List<Tuple> res = read(template);
        
        // remove them from space
        for (Tuple tuple : res) {
            space.remove(tuple);
        }

        return res;
    }

    @Override
    public List<Tuple> read(Template template) throws RemoteException {
        List<Tuple> res = new ArrayList<Tuple>();

        // search for matching tuples
        for (Tuple tuple : space) {
            if (template.matches(tuple))
                res.add(tuple);
        }

        return res;
    }
    
}
