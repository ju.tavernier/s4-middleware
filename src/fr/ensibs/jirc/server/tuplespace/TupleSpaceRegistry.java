package fr.ensibs.jirc.server.tuplespace;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import fr.ensibs.jirc.commons.tuplespace.TupleSpace;

public class TupleSpaceRegistry {

    Map<String, TupleSpace> spaces;

    public TupleSpaceRegistry() {
        spaces = new HashMap<String, TupleSpace>();
    }

    public TupleSpace getSpace(String name) throws RemoteException {
        // search for space
        TupleSpace space = spaces.get(name);
        // if doesn't exists, create it
        if (space == null) {
            space = new TupleSpaceImpl();
            spaces.put(name, space);
        }
        return space;
    }
    
}
