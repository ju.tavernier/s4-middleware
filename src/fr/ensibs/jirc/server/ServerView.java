package fr.ensibs.jirc.server;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import fr.ensibs.jirc.commons.view.View;
import fr.ensibs.jirc.commons.view.ViewInputListener;

public class ServerView extends View {
    
    public ServerView(ViewInputListener business) throws IOException {
        super(business);
        setStatus("Starting server...");
    }

    public void setStatus(int users, int rooms) {
        StringBuilder sb = new StringBuilder();
        
        // users count
        if (users == 0)
            sb.append("No connected user");
        else if (users == 1)
            sb.append("1 connected user");
        else
            sb.append(users + " connected users");
        sb.append(", ");

        // rooms count
        if (rooms == 0)
            sb.append("no active room");
        else if (rooms == 1)
            sb.append("1 active room");
        else
            sb.append(rooms + " active rooms");

        // current time
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        sb.append(" at " + dtf.format(now));

        super.setStatus(sb.toString());
    }

}
