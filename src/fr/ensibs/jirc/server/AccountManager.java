package fr.ensibs.jirc.server;

import java.util.Map;
import java.util.Random;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Arrays;
import java.util.HashMap;

public class AccountManager {
    
    //#region singleton pattern
    private static AccountManager instance;

    public static AccountManager getInstance() {
        if (instance == null)
            instance = new AccountManager();
        
        return instance;
    }
    //#endregion

    private Map<String, byte[]> users;
    private Algorithm tokenAlgorithm;
    private JWTVerifier tokenVerifier;
    
    private AccountManager() {
        users = new HashMap<String, byte[]>();

        // generate key for tokens
        byte[] key = new byte[512];
        (new Random()).nextBytes(key);
        tokenAlgorithm = Algorithm.HMAC512(key);

        tokenVerifier = JWT.require(tokenAlgorithm).build();
    }

    /**
     * 
     * @param username
     * @param password sha512 digest of password
     * @return
     */
    public boolean registerUser(String username, byte[] password) {
        // check if user exists
        if (users.get(username) != null)
            return false;
        else {
            users.put(username, password);
            return true;
        }
    }

    public String createToken(String username, byte[] password) {
        // check if user exists and password is valid
        byte[] localPassword = users.get(username);
        if (Arrays.equals(password, localPassword)) {
            // create and return token
            return JWT.create()
                .withSubject(username)
                .sign(tokenAlgorithm);
        } else
            return null;
    }

    public DecodedJWT validateToken(String token) {
        try {            
            return tokenVerifier.verify(token);
        } catch (Exception e) {
            return null;
        }
    }

}
