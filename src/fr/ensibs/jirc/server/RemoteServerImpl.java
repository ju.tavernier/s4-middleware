package fr.ensibs.jirc.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.TextMessage;

import com.auth0.jwt.interfaces.DecodedJWT;

import fr.ensibs.jirc.commons.ExceptionHandler;
import fr.ensibs.jirc.commons.RemoteServer;
import fr.ensibs.jirc.commons.tuplespace.Template;
import fr.ensibs.jirc.commons.tuplespace.Tuple;
import fr.ensibs.jirc.commons.tuplespace.TupleSpace;
import fr.ensibs.jirc.server.tuplespace.TupleSpaceImpl;
import fr.ensibs.jirc.server.tuplespace.TupleSpaceRegistry;

public class RemoteServerImpl implements RemoteServer {

    private ServerView view;
    private Registry reg;
    private TupleSpaceRegistry db;
    private ConnectionFactory connFactory;
    private JMSContext jmsContext;
    private Destination topic;
    private JMSProducer producer;
    private Map<String, TupleSpace> exports;

    //#region initialization
    public RemoteServerImpl(ServerView view, ConnectionFactory connFactory, Destination topic) {
        this.view = view;
        this.connFactory = connFactory;
        this.topic = topic;
        exports = new HashMap<String, TupleSpace>();
        init();
    }

    public void init() {
        // create RMI registry
        try {
            reg = LocateRegistry.createRegistry(8666);
        } catch (RemoteException e) {
            ExceptionHandler.exit(e, "Registry creation failed");
        }
        
        //create and export server
        RemoteServer serverStub = null;
        try {
            serverStub = (RemoteServer) UnicastRemoteObject.exportObject(this, 0);
        } catch (RemoteException e) {
            ExceptionHandler.exit(e, "Exporting RemoteServer failed");
        }
        try {
            reg.rebind("server", serverStub);
        } catch (RemoteException e) {
            ExceptionHandler.exit(e, "Binding RemoteServer to registry failed");
        }

        // create and export database
        db = new TupleSpaceRegistry();

        // jms
        if (connFactory == null || topic == null)
            ExceptionHandler.exit(new Exception(), "JMS initialization failed");
        jmsContext = connFactory.createContext(JMSContext.AUTO_ACKNOWLEDGE);
        producer = jmsContext.createProducer();
    }
    //#endregion

    //#region view's status update
    private final Runnable STATUS = new Runnable() {
        @Override public void run() {
            view.setStatus(usersCount(), roomsCount());
        }
    };

    private int usersCount() {
        try {
            TupleSpace users = db.getSpace("users");
            Template query = new Template(Template.WILDCARD);
            List<Tuple> res = users.read(query);
            return res.size();
        } catch (RemoteException e) { } // won't happen since db is in the same JVM
        return -1;
    }

    public List<String> getRooms() throws RemoteException {
        // tuple space "rooms" contains (room name, nickname) tuples
        Template query = new Template(Template.WILDCARD, Template.WILDCARD);
        List<Tuple> res = db.getSpace("rooms").read(query);
        List<String> rooms = res.stream()
            .map(i -> (String) i.get(0))
            .distinct()
            .collect(Collectors.toList());
        return rooms;
    }

    private int roomsCount() {
        try {
            return this.getRooms().size();
        } catch (RemoteException e) { } // won't happen since db is in the same JVM
        return -1;
    }
    //#endregion

    @Override
    public String getDisplayName() throws RemoteException {
        return "TestServer";
    }

    @Override
    public String getMotD() throws RemoteException {
        return "Wilkommen";
    }

    @Override
    public TupleSpace getDB(String name) throws RemoteException {
        TupleSpace stub = exports.get(name);
        if (stub == null) {
            TupleSpace space = db.getSpace(name);
            stub = (TupleSpace) UnicastRemoteObject.exportObject(space, 0);
            exports.put(name, stub);
        }
        return stub;
    }

    @Override
    public boolean register(String username, byte[] password) {
        boolean res = AccountManager.getInstance().registerUser(username, password);
        if (res)
            view.thread.invokeLater(REGISTER_SUCCESS);
        else
            view.thread.invokeLater(REGISTER_FAILURE);
        return res;
    }
    //#region register runnables
    private final Runnable REGISTER_SUCCESS = new Runnable() {
        @Override public void run() {
            view.addLog("[register] success");
        }
    };
    private final Runnable REGISTER_FAILURE = new Runnable() {
        @Override public void run() {
            view.addLog("[register] failure");
        }
    };
    //#endregion

    @Override
    public String auth(String username, byte[] password) throws RemoteException {
        // generate token
        String token = AccountManager.getInstance().createToken(username, password);
        // if creds were valid
        if (token != null) {
            // add user to connected users
            DecodedJWT validToken = AccountManager.getInstance().validateToken(token);
            ((TupleSpaceImpl) db.getSpace("users")).add(new Tuple(validToken.getSubject()));
            // update view
            view.thread.invokeLater(AUTH_SUCCESS);
            view.thread.invokeLater(STATUS);
        } else
            view.thread.invokeLater(AUTH_FAILURE);
        return token;
    }
    //#region auth runnables
    private final Runnable AUTH_SUCCESS = new Runnable() {
        @Override public void run() {
            view.addLog("[auth] success");
        }
    };
    private final Runnable AUTH_FAILURE = new Runnable() {
        @Override
        public void run() {
            view.addLog("[auth] failure");
        }
    };
    //#endregion

    @Override
    public boolean join(String token, String room) throws RemoteException {
        // FIXME room name and username shouldn't contain a pipe | since it's used as a field separator
        // validate token
        DecodedJWT validToken = AccountManager.getInstance().validateToken(token);
        if (validToken != null) {
            // check if user connected
            Template query = new Template(validToken.getSubject());
            TupleSpaceImpl space = (TupleSpaceImpl) db.getSpace("users");
            List<Tuple> res = space.read(query);
            if (res.size() > 0) {
                // check if user already in this room
                query = new Template(room, validToken.getSubject());
                space = (TupleSpaceImpl) db.getSpace("rooms");
                res = space.read(query);
                if (res.size() == 0) {
                    // register user to room
                    space.add(new Tuple(room, validToken.getSubject()));
                    view.thread.invokeLater(JOIN_SUCCESS);
                    view.thread.invokeLater(STATUS);
                    // send message to topic
                    TextMessage msg = jmsContext.createTextMessage(room + "|" + validToken.getSubject() + "|join");
                    producer.send(topic, msg);
                    return true;
                }
            }
        }
        view.thread.invokeLater(JOIN_FAILURE);
        return false;
    }
    //#region join runnables
    private final Runnable JOIN_SUCCESS = new Runnable() {
        @Override public void run() {
            view.addLog("[join] success");
        }
    };
    private final Runnable JOIN_FAILURE = new Runnable() {
        @Override public void run() {
            view.addLog("[join] failure");
        }
    };
    //#endregion

    @Override
    public boolean sendMsg(String token, String room, String msg) throws RemoteException {
        // validate token
        DecodedJWT validToken = AccountManager.getInstance().validateToken(token);
        if (validToken != null) {
            // check if user is in room
            Template query = new Template(room, validToken.getSubject());
            TupleSpaceImpl space = (TupleSpaceImpl) db.getSpace("rooms");
            List<Tuple> res = space.read(query);
            if (res.size() == 1) {
                // send message
                TextMessage jmsMsg = jmsContext.createTextMessage(room + "|" + validToken.getSubject() + "|message|" + msg);
                producer.send(topic, jmsMsg);
                // save message to db
                long timestamp = System.currentTimeMillis() / 1000L;
                Tuple dbMsg = new Tuple(timestamp, validToken.getSubject(), msg);
                space = (TupleSpaceImpl) db.getSpace("room-" + room);
                space.add(dbMsg);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean leave(String token, String room) throws RemoteException {
        // validate token
        DecodedJWT validToken = AccountManager.getInstance().validateToken(token);
        if (validToken != null) {
            // check if user is in room
            Template query = new Template(room, validToken.getSubject());
            TupleSpaceImpl space = (TupleSpaceImpl) db.getSpace("rooms");
            List<Tuple> res = space.read(query);
            if (res.size() == 1) {
                // unregister user from room
                space.take(query);
                view.thread.invokeLater(LEAVE_SUCCESS);
                view.thread.invokeLater(STATUS);
                // send message to topic
                TextMessage msg = jmsContext.createTextMessage(room + "|" + validToken.getSubject() + "|leave");
                producer.send(topic, msg);
                return true;
            }
        }
        view.thread.invokeLater(LEAVE_FAILURE);
        return false;
    }
    //#region leave runnables
    private final Runnable LEAVE_SUCCESS = new Runnable() {
        @Override public void run() {
            view.addLog("[leave] success");
        }
    };
    private final Runnable LEAVE_FAILURE = new Runnable() {
        @Override public void run() {
            view.addLog("[leave] failure");
        }
    };
    //#endregion

    @Override
    public boolean disconnect(String token) throws RemoteException {
        // validate token
        DecodedJWT validToken = AccountManager.getInstance().validateToken(token);
        // remove from connected users
        if (validToken != null) {
            // update db
            ((TupleSpaceImpl) db.getSpace("users")).take(new Template(validToken.getSubject()));
            // update view
            view.thread.invokeLater(DISCONNECT_SUCCESS);
            view.thread.invokeLater(STATUS);
            return true;
        } else {
            view.thread.invokeLater(DISCONNECT_FAILURE);
            return false;
        }
    }
    //#region disconnect runnables
    private final Runnable DISCONNECT_SUCCESS = new Runnable() {
        @Override public void run() {
            view.addLog("[disconnect] success");
        }
    };
    private final Runnable DISCONNECT_FAILURE = new Runnable() {
        @Override public void run() {
            view.addLog("[disconnect] failure");
        }
    };
    //#endregion

}
