package fr.ensibs.jirc.server;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;

import fr.ensibs.jirc.commons.ExceptionHandler;
import fr.ensibs.jirc.commons.RemoteServer;
import fr.ensibs.jirc.commons.view.ViewInputListener;

public class ServerApp implements ViewInputListener {
    
    //#region main
    /**
     * Server's entry point
     * @param args command line args, not used
     */
    public static void main(String[] args) {
        new ServerApp();
    }
    //#endregion

    //#region server
    private ServerView view;
    private RemoteServer server;
    
    @Resource(lookup = "java:comp/DefaultJMSConnectionFactory")
    private static ConnectionFactory connFactory;

    @Resource(lookup = "jms/jirc")
    private static Destination topic;

    public ServerApp() {
        // create view
        try {
            view = new ServerView(this);
        } catch (IOException e) {
            ExceptionHandler.exit(e, "View constructor failed");
        }

        // create server
        server = new RemoteServerImpl(view, connFactory, topic);

        // if program hasn't crashed during init
        view.thread.invokeLater(INIT_SUCCESS);
    }

    private final Runnable INIT_SUCCESS = new Runnable() {
        @Override public void run() {
            try {
                view.setStatus("Server " + server.getDisplayName() + " started successfully");
            } catch (RemoteException e) { /* won't fail, object isn't remote */ }
        }
    };
    //#endregion

    //#region commands
    @Override
    public void parseInput(String input) {
        // this can only be commands
        String[] cmd = input.split(" ");
        switch (cmd[0]) {
            case "exit":
                view.close();
                System.exit(0);
            case "help":
                // general help
                if (cmd.length == 1)
                    view.thread.invokeLater(HELP);
                // help about a command
                else
                    cmdHelp(cmd[1]);
                break;
            default:
                view.thread.invokeLater(UNKNOWN);
        }
    }
    //#endregion

    //#region help command
    private void cmdHelp(String cmd) {
        Runnable res;
        switch (cmd) {
            case "which":
                res = HELP_WHICH;
                break;
            case "who":
                res = HELP_WHO;
                break;
            case "rooms":
                res = HELP_ROOMS;
                break;
            case "users":
                res = HELP_USERS;
                break;
            default:
                res = UNKNOWN;
        }
        view.thread.invokeLater(res);
    }

    private final Runnable UNKNOWN = new Runnable() {
        @Override public void run() {
            view.setCmdResult("Unknown command");
        }
    };
    private final Runnable HELP = new Runnable() {
        @Override public void run() {
            view.setCmdResult("Available commands: which, who, rooms, users (try '/help <cmd>')");
        }
    };
    private final Runnable HELP_WHICH = new Runnable() {
        @Override public void run() {
            view.setCmdResult("In which room is a given user: /which <nickname>");
        }
    };
    private final Runnable HELP_WHO = new Runnable() {
        @Override public void run() {
            view.setCmdResult("Which users are in a given room: /who <room_name>");
        }
    };
    private final Runnable HELP_ROOMS = new Runnable() {
        @Override public void run() {
            view.setCmdResult("List active rooms: /rooms");
        }
    };
    private final Runnable HELP_USERS = new Runnable() {
        @Override public void run() {
            view.setCmdResult("List connected users: /users");
        }
    };
    //#endregion
}
