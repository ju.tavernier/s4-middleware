package fr.ensibs.jirc.client;

import java.io.IOException;

import fr.ensibs.jirc.commons.view.View;
import fr.ensibs.jirc.commons.view.ViewInputListener;

public class ClientView extends View {

    public ClientView(ViewInputListener business) throws IOException {
        super(business);
        setStatus("Welcome! '/connect' to a server and '/auth' yourself, or ask for '/help' if you feel lost");
    }
    
}
