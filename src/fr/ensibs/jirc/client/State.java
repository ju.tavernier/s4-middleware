package fr.ensibs.jirc.client;

public enum State {
    DISCONNECTED,
    CONNECTED,
    AUTHENTICATED,
    IN_A_ROOM
}
