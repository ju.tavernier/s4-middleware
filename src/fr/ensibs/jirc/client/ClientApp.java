package fr.ensibs.jirc.client;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.auth0.jwt.JWT;

import fr.ensibs.jirc.commons.ExceptionHandler;
import fr.ensibs.jirc.commons.RemoteServer;
import fr.ensibs.jirc.commons.tuplespace.Template;
import fr.ensibs.jirc.commons.tuplespace.Tuple;
import fr.ensibs.jirc.commons.tuplespace.TupleSpace;
import fr.ensibs.jirc.commons.view.ViewInputListener;

public class ClientApp implements ViewInputListener, MessageListener {

    //#region main
    /**
     * Client's entry point
     * @param args command line args, not used
     */
    public static void main(String[] args) {
        new ClientApp();
    }
    //#endregion

    //#region client
    private ClientView view;
    private Registry reg;
    private RemoteServer server;
    private State state;
    private String token;
    private String currentRoom;

    @Resource(lookup = "java:comp/DefaultJMSConnectionFactory")
    private static ConnectionFactory connFactory;

    @Resource(lookup = "jms/jirc")
    private static Destination topic;

    public ClientApp() {
        // setup view
        try {
            view = new ClientView(this);
        } catch (IOException e) {
            ExceptionHandler.exit(e, "View constructor failed");
        }

        // server has to be connected with a command
        reg = null;
        server = null;
        state = State.DISCONNECTED;
        currentRoom = null;

        // check jms
        if (connFactory == null || topic == null)
            ExceptionHandler.exit(new Exception(), "JMS initialization failed");

        // register self as a message consumer
        JMSContext jmsContext = connFactory.createContext(JMSContext.AUTO_ACKNOWLEDGE);
        JMSConsumer consumer = jmsContext.createConsumer(topic);
        consumer.setMessageListener(this);
    }

    @Override
    public void parseInput(String input) {
        if (input.length() > 0) {
            // command or message?
            if (input.substring(0, 1).equals("/")) { // this is a command
                String[] cmd = input.substring(1).split(" ");
                if (! cmd[0].equals(""))
                    parseCommand(cmd);
            } else { // this is a message
                sendMessage(input);
            }
        }
    }
    //#endregion

    //#region messaging
    private void sendMessage(String msg) {
        if (state == State.IN_A_ROOM) { // user is in a room
            boolean res = false;
            try {
                res = server.sendMsg(token, currentRoom, msg);
            } catch (RemoteException e) {
                ExceptionHandler.connectionFailed(e);
            }
            if (!res)
                view.thread.invokeLater(MSG_FAILURE);
        } else // user hasn't joined a room
            view.thread.invokeLater(HASNT_JOINED_ROOM);
    }
    //#region sendmessage runnables
    private final Runnable MSG_FAILURE = new Runnable() {
        @Override public void run() {
            view.setCmdResult("Error while sending message");
        }
    };
    //#endregion

    @Override
    public void onMessage(Message arg0) {
        String[] msg = null;
        try {
            msg = ((TextMessage) arg0).getText().split("\\|");
        } catch (JMSException e) {
            ExceptionHandler.exit(e, "Failed to read JMS message");
        }

        String room = msg[0];
        String subject = msg[1];
        String type = msg[2];
        // check room
        if (state == State.IN_A_ROOM)
            if (currentRoom.equals(room)) {
                // check message type
                switch (type) {
                    case "join":
                        view.thread.invokeLater(new Runnable() {
                            @Override public void run() {
                                view.addLog("#" + room + " -> " + subject + " joined the room");
                            }
                        });
                        break;
                    case "leave":
                        view.thread.invokeLater(new Runnable() {
                            @Override public void run() {
                                view.addLog("#" + room + " -> " + subject + " left the room");
                            }
                        });
                        break;
                    case "message":
                        String[] content = Arrays.copyOfRange(msg, 3, msg.length);
                        view.thread.invokeLater(new Runnable() {
                            @Override public void run() {
                                // String.join to cancel the original split of the message in case message contained '|' char
                                view.addLog("#" + room + " " + subject + ": " + String.join("|", content));
                            }
                        });
                        break;
                    default:
                        view.thread.invokeLater(new Runnable() {
                            @Override public void run() {
                                view.addLog("[warning] Received a message of unknown type: " + type);
                            }
                        });
                }
            }
    }

    private final Runnable HASNT_JOINED_ROOM = new Runnable() {
        @Override public void run() {
            view.setStatus("You haven't joined a room yet (try '/help' if you're lost)");
        }
    };
    //#endregion

    //#region commands
    private void parseCommand(String[] cmd) {
        switch (cmd[0]) {
            case "exit":
                exit();
                break;
            case "help":
                help(cmd);
                break;
            case "connect":
                connect(cmd);
                break;
            case "register":
                register(cmd);
                break;
            case "auth":
                auth(cmd);
                break;
            case "list":
                list(cmd);
                break;
            case "who":
                who(cmd);
                break;
            case "which":
                which(cmd);
                break;
            case "join":
                join(cmd);
                break;
            case "leave":
                leave();
                break;
            case "disconnect":
                disconnect();
                break;
            default:
                view.thread.invokeLater(UNKNOWN);
        }
    }

    private void exit() {
        if (state != State.DISCONNECTED)
            disconnect();
        view.close();
        System.exit(0);
    }

    private void connect(String[] args) {
        // should be "/connect address port"
        if (args.length == 3) {
            // if not already connected somewhere
            if (state != State.CONNECTED) {
                // connect to server and check registry
                boolean connectionOk;
                try {
                    reg = LocateRegistry.getRegistry(args[1], Integer.parseInt(args[2]));
                    server = (RemoteServer) reg.lookup("server");
                    connectionOk = true;
                } catch (Exception e) {
                    connectionOk = false;
                    if (e instanceof NumberFormatException)
                        view.thread.invokeLater(CONNECT_PORT_NOT_NUMBER);
                    else if (e instanceof RemoteException)
                        view.thread.invokeLater(CONNECT_CONTACT_FAIL);
                    else // NotBoundException
                        view.thread.invokeLater(CONNECT_LOOKUP_FAIL);
                }

                // everything went well
                if (connectionOk) {
                    state = State.CONNECTED;
                    view.thread.invokeLater(CONNECT_SUCCESS);
                    view.thread.invokeLater(MOTD);
                    view.thread.invokeLater(STATUS);
                // something has failed, cleanup
                } else {
                    server = null;
                    reg = null;
                }
            } else
                view.thread.invokeLater(CONNECT_ALREADY_CONNECTED);
        } else
            cmdHelp("connect");
    }
    //#region connect runnables
    private final Runnable CONNECT_ALREADY_CONNECTED = new Runnable() {
        @Override public void run() {
            view.setCmdResult("connect: you're already connected to a server");
        }
    };
    private final Runnable CONNECT_PORT_NOT_NUMBER = new Runnable() {
        @Override public void run() {
            view.setCmdResult("connect: port has to be a number");
        }
    };
    private final Runnable CONNECT_CONTACT_FAIL = new Runnable() {
        @Override public void run() {
            view.setCmdResult("connect: couldn't reach server");
        }
    };
    private final Runnable CONNECT_LOOKUP_FAIL = new Runnable() {
        @Override public void run() {
            view.setCmdResult("connect: couldn't retrieve server information");
        }
    };
    private final Runnable CONNECT_SUCCESS = new Runnable() {
        @Override public void run() {
            view.setCmdResult("connect: success");
        }
    };
    //#endregion

    // used by register() and auth()
    private byte[] passwordToHash(String password) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) { /* won't happen */ }
        return md.digest(password.getBytes());
    }

    private void register(String[] args) {
        // should be "/register nickname password"
        if (args.length == 3) {
            if (state != State.DISCONNECTED) {
                String nickname = args[1];
                byte[] password = passwordToHash(args[2]);
                // register to server
                try {
                    if (server.register(nickname, password))
                        view.thread.invokeLater(REGISTER_SUCCESS);
                    else
                        view.thread.invokeLater(REGISTER_FAILURE);
                } catch (RemoteException e) {
                    ExceptionHandler.connectionFailed(e);
                }
            } else
                view.thread.invokeLater(REGISTER_NOT_CONNECTED);
        } else
            cmdHelp("register");
    }
    //#region register runnables
    private final Runnable REGISTER_NOT_CONNECTED = new Runnable() {
        @Override public void run() {
            view.setCmdResult("register: you have to be connected to a server (see /connect)");
        }
    };
    private final Runnable REGISTER_FAILURE = new Runnable() {
        @Override public void run() {
            view.setCmdResult("register: failed, maybe nickname is already used");
        }
    };
    private final Runnable REGISTER_SUCCESS = new Runnable() {
        @Override public void run() {
            view.setCmdResult("register: success");
        }
    };
    //#endregion

    private void auth(String[] args) {
        // should be "/auth username password"
        if (args.length == 3) {
            // check state
            if (state == State.DISCONNECTED)
                view.thread.invokeLater(AUTH_NOT_CONNECTED);
            else if (state != State.CONNECTED)
                view.thread.invokeLater(AUTH_ALREADY_AUTHENTICATED);
            else {
                String nickname = args[1];
                byte[] password = passwordToHash(args[2]);
                try {
                    token = server.auth(nickname, password);
                } catch (RemoteException e) {
                    ExceptionHandler.connectionFailed(e);
                }
                if (token != null) {
                    state = State.AUTHENTICATED;
                    view.thread.invokeLater(STATUS);
                    view.thread.invokeLater(AUTH_SUCCESS);
                } else
                    view.thread.invokeLater(AUTH_FAILURE);
            }
        } else
            cmdHelp("auth");
    }
    //#region auth runnables
    private final Runnable AUTH_NOT_CONNECTED = new Runnable() {
        @Override public void run() {
            view.setCmdResult("auth: you have to be connected to a server to authenticate");
        }
    };
    private final Runnable AUTH_ALREADY_AUTHENTICATED = new Runnable() {
        @Override public void run() {
            view.setCmdResult("auth: you are already authenticated");
        }
    };
    private final Runnable AUTH_SUCCESS = new Runnable() {
        @Override public void run() {
            view.setCmdResult("auth: success");
        }
    };
    private final Runnable AUTH_FAILURE = new Runnable() {
        @Override public void run() {
            view.setCmdResult("auth: failure (have you registered yet?)");
        }
    };
    //#endregion

    private void list(String[] args) {
        // should be /list (rooms|users)
        if (args.length == 2) {
            if (state != State.DISCONNECTED) {
                String firstChar = args[1].substring(0, 1);
                if (firstChar.equals("r")) { // list rooms
                    listRooms();
                } else if (firstChar.equals("u")) { // list users
                    listUsers();
                } else
                    cmdHelp("list");
            } else
                view.thread.invokeLater(LIST_NOT_CONNECTED);            
        } else
            cmdHelp("list");
    }
    //#region list subfunctions
    private void listRooms() {
        // tuple space "rooms" contains (room name, nickname) tuples
        Template query = new Template(Template.WILDCARD, Template.WILDCARD);
        List<Tuple> result = null;
        try {
            TupleSpace roomsSpace = server.getDB("rooms");
            result = roomsSpace.read(query);
        } catch (RemoteException e) {
            ExceptionHandler.connectionFailed(e);
        }
        // from list of tuples to list of room names
        List<String> rooms = result.stream()
            .map(i -> (String) i.get(0))
            .distinct()
            .collect(Collectors.toList());
        // format list
        StringBuilder roomsList = new StringBuilder();
        if (rooms.size() > 0) {
            for (String room : rooms) {
                roomsList.append(room);
                roomsList.append(", ");
            }
            roomsList.delete(roomsList.length() - 2, roomsList.length() - 1);
        } else
            roomsList.append("-");
        // display message
        view.thread.invokeLater(new Runnable() {
            @Override public void run() {
                view.setCmdResult("active rooms: " + roomsList.toString());
            }
        });
    }

    private void listUsers() {
        // tuple space "users" contains (nickname) tuples
        Template query = new Template(Template.WILDCARD);
        List<Tuple> result = null;
        try {
            TupleSpace usersSpace = server.getDB("users");
            result = usersSpace.read(query);
        } catch (RemoteException e) {
            ExceptionHandler.connectionFailed(e);
        }
        // from list of tuples to list of usernames
        List<String> users = result.stream()
            .map(i -> (String) i.get(0))
            .collect(Collectors.toList());
        // format list
        StringBuilder usersList = new StringBuilder();
        if (users.size() > 0) {
            for (String user : users) {
                usersList.append(user);
                usersList.append(", ");
            }
            usersList.delete(usersList.length() - 2, usersList.length() - 1);
        } else
            usersList.append("-");
        // display message
        view.thread.invokeLater(new Runnable() {
            @Override public void run() {
                view.setCmdResult("connected users: " + usersList.toString());
            }
        });
    }
    //#endregion
    //#region list runnables
    private final Runnable LIST_NOT_CONNECTED = new Runnable() {
        @Override public void run() {
            view.setCmdResult("list: you have to be connected to a server to list its users and rooms");
        }
    };
    //#endregion

    private void who(String[] args) {
        // should be "/who <room>"
        if (args.length == 2) {
            Template request = new Template(args[1], Template.WILDCARD);
            List<Tuple> res = null;
            try {
                TupleSpace db = server.getDB("rooms");
                res = db.read(request);
            } catch (RemoteException e) {
                ExceptionHandler.connectionFailed(e);
            }
            // test if room active
            if (res.size() == 0) {
                view.thread.invokeLater(new Runnable() {
                    @Override public void run() {
                        view.setCmdResult("who: room '" + args[1] + "' is not active");
                    }
                });
            } else {
                // from tuple list to username list
                List<String> users = res.stream()
                    .map(i -> (String) i.get(1))
                    .collect(Collectors.toList());
                if (users.size() > 1) {
                    // build message
                    StringBuilder sb = new StringBuilder();
                    for (String user : users) {
                        sb.append(user);
                        sb.append(", ");
                    }
                    sb.delete(sb.length() - 2, sb.length() - 1);
                    String message = sb.toString();
                    // display message
                    view.thread.invokeLater(new Runnable() {
                        @Override public void run() {
                            view.setCmdResult("who: " + message + "are in room '" + args[1] + "'");
                        }
                    });
                } else {
                    view.thread.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            view.setCmdResult("who: " + users.get(0) + " is in room '" + args[1] + "'");
                        }
                    });
                }
            }
        } else
            cmdHelp("who");
    }

    private void which(String[] args) {
        // should be "/which <username>"
        if (args.length == 2) {
            // test if user is connected
            Template query = new Template(args[1]);
            List<Tuple> res = null;
            try {
                TupleSpace db = server.getDB("users");
                res = db.read(query);
            } catch (RemoteException e) {
                ExceptionHandler.connectionFailed(e);
            }
            if (res.size() > 0) {
                // search for room with this user
                query = new Template(Template.WILDCARD, args[1]);
                try {
                    TupleSpace db = server.getDB("rooms");
                    res = db.read(query);
                } catch (RemoteException e) {
                    ExceptionHandler.connectionFailed(e);
                }
                if (res.size() > 0) {
                    String room = (String) res.get(0).get(0);
                    view.thread.invokeLater(new Runnable() {
                        @Override public void run() {
                            view.setCmdResult("which: user '" + args[1] + "' is in room '" + room + "'");
                        }
                    });
                } else {
                    view.thread.invokeLater(new Runnable() {
                        @Override public void run() {
                            view.setCmdResult("which: user '" + args[1] + "' isn't in a room");
                        }
                    });
                }
            } else {
                view.thread.invokeLater(new Runnable() {
                    @Override public void run() {
                        view.setCmdResult("which: user '" + args[1] + "' isn't connected");
                    }
                });
            }
        } else
            cmdHelp("which");
    }

    private void join(String[] args) {
        // should be "/join <room>"
        if (args.length == 2) {
            if (state == State.AUTHENTICATED) {
                boolean res = false;
                try {
                    res = server.join(token, args[1]);
                } catch (RemoteException e) {
                    ExceptionHandler.connectionFailed(e);
                }
                if (res) {
                    state = State.IN_A_ROOM;
                    currentRoom = args[1];
                    view.thread.invokeLater(STATUS);
                    view.thread.invokeLater(JOIN_SUCCESS);
                    // retrieve room history
                    Template query = new Template(Template.WILDCARD, Template.WILDCARD, Template.WILDCARD);
                    List<Tuple> history = null;
                    try {
                        TupleSpace space = server.getDB("room-" + currentRoom);
                        history = space.read(query);
                    } catch (RemoteException e) {
                        ExceptionHandler.connectionFailed(e);
                    }
                    // if history not empty, sort and display
                    if (history.size() > 0) {
                        history.sort((t1, t2) -> ((Long) t1.get(0)).compareTo((Long) t2.get(0)));
                        final List<Tuple> fHistory = new ArrayList<Tuple>(history);
                        view.thread.invokeLater(new Runnable() {
                            @Override public void run() {
                                for (Tuple msg : fHistory) {
                                    view.addLog("#" + currentRoom + " " + msg.get(1) + ": " + msg.get(2));
                                }
                            }
                        });
                    }
                } else
                    view.thread.invokeLater(JOIN_FAILURE);
            } else if (state == State.IN_A_ROOM)
                view.thread.invokeLater(JOIN_ALREADY_IN_ROOM);
            else
                view.thread.invokeLater(JOIN_NOT_AUTHENTICATED);
        } else
            cmdHelp("join");
    }
    //#region join runnables
    private final Runnable JOIN_NOT_AUTHENTICATED = new Runnable() {
        @Override public void run() {
            view.setCmdResult("join: you have to be authenticated to join a room");
        }
    };
    private final Runnable JOIN_ALREADY_IN_ROOM = new Runnable() {
        @Override public void run() {
            view.setCmdResult("join: you're already in a room");
        }
    };
    private final Runnable JOIN_SUCCESS = new Runnable() {
        @Override public void run() {
            view.setCmdResult("join: success");
        }
    };
    private final Runnable JOIN_FAILURE = new Runnable() {
        @Override public void run() {
            view.setCmdResult("join: failure");
        }
    };
    //#endregion

    private void leave() {
        if (state == State.IN_A_ROOM) {
            boolean res = false;
            try {
                res = server.leave(token, currentRoom);
            } catch (RemoteException e) {
                ExceptionHandler.connectionFailed(e);
            }
            if (res) {
                state = State.AUTHENTICATED;
                currentRoom = null;
                view.thread.invokeLater(CLEAR_LOGS);
                view.thread.invokeLater(STATUS);
                view.thread.invokeLater(LEAVE_SUCCESS);
                try { Thread.sleep(5); } catch (InterruptedException e) { } // c'est moche mais sinon ça clear après le motd...
                view.thread.invokeLater(MOTD);
            } else
                view.thread.invokeLater(LEAVE_FAILURE);
        } else
            view.thread.invokeLater(LEAVE_NOT_IN_A_ROOM);
    }
    //#region leave runnables
    private final Runnable LEAVE_NOT_IN_A_ROOM = new Runnable() {
        @Override public void run() {
            view.setCmdResult("leave: you are not in a room");
        }
    };
    private final Runnable LEAVE_SUCCESS = new Runnable() {
        @Override public void run() {
            view.setCmdResult("leave: success");
        }
    };
    private final Runnable LEAVE_FAILURE = new Runnable() {
        @Override public void run() {
            view.setCmdResult("leave: failure");
        }
    };
    //#endregion

    private void disconnect() {
        if (state == State.IN_A_ROOM)
            leave();
        if (state == State.AUTHENTICATED) {
            boolean flag = false;
            try {
                flag = server.disconnect(token);
            } catch (RemoteException e) {
                ExceptionHandler.connectionFailed(e);
            }
            if (flag) { //success
                token = null;
                state = State.CONNECTED;        
            } else {
                view.thread.invokeLater(DISCONNECT_FAILURE);
            }
        }
        if (state == State.CONNECTED) {
            reg = null;
            server = null;
            state = State.DISCONNECTED;
            view.thread.invokeLater(DISCONNECT_SUCCESS);
            view.thread.invokeLater(STATUS);
            view.thread.invokeLater(CLEAR_LOGS);
        } else
            view.thread.invokeLater(DISCONNECT_NOT_CONNECTED);
    }
    //#region disconnect runnables
    private final Runnable DISCONNECT_SUCCESS = new Runnable() {
        @Override public void run() {
            view.setCmdResult("disconnect: success");
        }
    };
    private final Runnable DISCONNECT_NOT_CONNECTED = new Runnable() {
        @Override public void run() {
            view.setCmdResult("disconnect: you aren't connected anywhere");
        }
    };
    private final Runnable DISCONNECT_FAILURE = new Runnable() {
        @Override public void run() {
            view.setCmdResult("disconnect: failure");
        }
    };
    //#endregion

    private void help(String[] args) {
        // general help
        if (args.length == 1)
            view.thread.invokeLater(HELP);
        // help about a command
        else
            cmdHelp(args[1]);
    }

    private final Runnable STATUS = new Runnable() {
        @Override public void run() {
            String nickname;
            switch (state) {
                case DISCONNECTED:
                    view.setStatus("Not connected to a server");
                    break;
                case CONNECTED:
                    try {
                        view.setStatus("Connected to " + server.getDisplayName() + ", not authenticated");
                    } catch (RemoteException e) {
                        ExceptionHandler.connectionFailed(e);
                    }
                    break;
                case AUTHENTICATED:
                    nickname = JWT.decode(token).getSubject();
                    try {
                        view.setStatus("Connected to " + server.getDisplayName() + " as " + nickname + ", not in a room");
                    } catch (RemoteException e) {
                        ExceptionHandler.connectionFailed(e);
                    }
                    break;
                case IN_A_ROOM:
                    nickname = JWT.decode(token).getSubject();
                    try {
                        view.setStatus("Connected to " + server.getDisplayName() + " as " + nickname + ", in room " + currentRoom);
                    } catch (RemoteException e) {
                        ExceptionHandler.connectionFailed(e);
                    }
                    break;
            }
        }
    };

    private final Runnable CLEAR_LOGS = new Runnable() {
        @Override public void run() {
            view.clearLogs();
        }
    };

    private final Runnable MOTD = new Runnable() {
        @Override public void run() {
            try {
                view.addLog(server.getMotD());
            } catch (RemoteException e) {
                ExceptionHandler.connectionFailed(e);
            }
        }
    };
    //#endregion

    //#region help command
    private void cmdHelp(String cmd) {
        Runnable res;
        switch (cmd) {
            case "connect":
                res = HELP_CONNECT;
                break;
            case "register":
                res = HELP_REGISTER;
                break;
            case "auth":
                res = HELP_AUTH;
                break;
            case "join":
                res = HELP_JOIN;
                break;
            case "leave":
                res = HELP_LEAVE;
                break;
            case "list":
                res = HELP_LIST;
                break;
            case "who":
                res = HELP_WHO;
                break;
            case "which":
                res = HELP_WHICH;
                break;
            case "disconnect":
                res = HELP_DISCONNECT;
                break;
            case "exit":
                res = HELP_EXIT;
                break;
            default:
                res = UNKNOWN;
        }
        view.thread.invokeLater(res);
    }

    private final Runnable UNKNOWN = new Runnable() {
        @Override public void run() {
            view.setCmdResult("Unknown command");
        }
    };
    private final Runnable HELP = new Runnable() {
        @Override public void run() {
            view.setCmdResult("Available commands: connect, register, auth, list, who, which, join, leave, disconnect, exit (try '/help <cmd>')");
        }
    };
    private final Runnable HELP_CONNECT = new Runnable() {
        @Override public void run() {
            view.setCmdResult("Connect to a JIRC server: /connect <address> <port>");
        };
    };
    private final Runnable HELP_REGISTER = new Runnable() {
        @Override public void run() {
            view.setCmdResult("Register an account on the server: /register <nickname> <password>");
        }
    };
    private final Runnable HELP_AUTH = new Runnable() {
        @Override public void run() {
            view.setCmdResult("Authenticate on the server: /auth <nickname> <password>");
        }
    };
    private final Runnable HELP_JOIN = new Runnable() {
        @Override public void run() {
            view.setCmdResult("Join a room on current server: /join <room_name>");
        }
    };
    private final Runnable HELP_LEAVE = new Runnable() {
        @Override public void run() {
            view.setCmdResult("Leave a room: /leave <room_name>");
        };
    };
    private final Runnable HELP_LIST = new Runnable() {
        @Override public void run() {
            view.setCmdResult("List active rooms and connected users: /list (rooms|users)");
        }
    };
    private final Runnable HELP_WHO = new Runnable() {
        @Override public void run() {
            view.setCmdResult("List users in a room: /who <room>");
        }
    };
    private final Runnable HELP_WHICH = new Runnable() {
        @Override public void run() {
            view.setCmdResult("Search in which room is a user: /which <username>");
        }
    };
    private final Runnable HELP_DISCONNECT = new Runnable() {
        @Override public void run() {
            view.setCmdResult("Leave current room if any and disconnect from server: /disconnect");
        }
    };
    private final Runnable HELP_EXIT = new Runnable() {
        @Override public void run() {
            view.setCmdResult("Disconnect and exit JIRC client: /exit");
        }
    };
    //#endregion

}
