#!/bin/bash

srcpath="$(pwd)"
buildpath="/tmp/middleware-$(date +%s)"

#####
echo "Clean old build"
rm -rf ./bin
mkdir ./bin

#####
echo "Compile project"
cd ./src
javac -d ../bin -classpath ".:../lib/jackson-annotations-2.13.2.jar:../lib/jackson-core-2.13.2.jar:../lib/jackson-databind-2.13.2.2.jar:../lib/javaee-api-8.0.1.jar:../lib/java-jwt-3.19.1.jar:../lib/lanterna-3.2.0-alpha1.jar" ./fr/ensibs/jirc/client/ClientApp.java ./fr/ensibs/jirc/server/ServerApp.java
cd ../

#####
echo "Copy source to $buildpath"
mkdir $buildpath
cp -R ./bin/* $buildpath

#####
echo "Copy and decompress libs to $buildpath"
cp ./lib/* $buildpath
cd $buildpath
rm ./*-sources.jar # we don't need libs sources in final jar
unzip -o "./*.jar"
rm ./*.jar

#####
echo "Clean libs manifests"
rm -r ./META-INF

#####
echo "Make server executable jar"
jar cfe ../jirc-server.jar fr.ensibs.jirc.server.ServerApp ./*

#####
echo "Make client executable jar"
jar cfe ../jirc-client.jar fr.ensibs.jirc.client.ClientApp ./*

#####
echo "Cleanup"
cd $srcpath
mv /tmp/jirc-*.jar ./bin
rm -r $buildpath
